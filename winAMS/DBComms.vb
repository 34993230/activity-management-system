﻿Imports System.Data.SQLite
Imports System.IO
Module DBComms
    Public Sub Init()
        Dim SQLDir As String = "ams.db" 'Add "ams.db" in order to check correctly
        If File.Exists(SQLDir) Then
            Debug.WriteLine("File Exists!!")
        Else
            GenerateDB()
            Debug.WriteLine("Database doesn't exist, creating one...")
        End If
    End Sub


    Public Sub GenerateDB()
        Dim DBConnection As SQLiteConnection
        SQLiteConnection.CreateFile("ams.db")
        DBConnection = New SQLiteConnection("Data Source=ams.db;Version=3;")
        DBConnection.Open()
        Dim Command As String
        Command = "CREATE TABLE Central (Firstname STRING, Surname STRING, StudentID STRING, Homeroom STRING, Activity STRING);"
        Dim Exec = New SQLiteCommand(Command, DBConnection)
        Exec.ExecuteNonQuery()
    End Sub
    'TODO: ADD DELETE, ADD, MODIFY FUNCTIONS. CHANGABLE VIA FUNCTION(****)
End Module